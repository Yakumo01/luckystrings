/**
 * Separate Interface allowing you to to define what makes a word 'lucky' 
 * This could be very helpful if you later change your mind about this, or perhaps somebody else 
 * reuses your solution
 */
public interface Criteria {
	
	/**
	 * Single method returns true if a word is lucky 
	 * @param s
	 * @return true if lucky, false otherwise
	 * @throws Exception we could be more specific about Exception type
	 */
	public boolean isLucky(String s) throws Exception;
}
