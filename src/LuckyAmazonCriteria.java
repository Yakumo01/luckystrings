
/**
 * This is the implementation of 'isLucky' criteria as required for this test
 * A word is lucky if it has at least 4 lucky letters
 * Lucky letters are 'a' and every second letter thereafter in the lower case alphabet
 *
 */
public class LuckyAmazonCriteria implements Criteria{

	/**
	 * Takes a string and checks if it conforms to the requirements given
	 * This will throw and exception if the input is not a single lowercase word
	 */
	@Override
	public boolean isLucky(String s) throws Exception{
		if ((s == null) || (s.isEmpty()))
			throw new Exception("lowercase whole-words only please");
		if (s.length()<4) //shortcut
			return false;
		
		// we could pre-check that the word contains only lower-case 'a'-'z'
		// but it is faster if we know that this is done for us
		
		String regex = "^[a-z]+$";
		if (!s.matches(regex))
			throw new Exception("lowercase whole-words only please");
		int luck = 0;
		
		
		for (int i=0;i<s.length();++i) {
			
			if (((s.charAt(i) - 'a') % 2) == 0) // the criterion given
			{
				luck++;
				if (luck == 4)
					return true; //skip rest
			}
		}
		return false;
	}

}
