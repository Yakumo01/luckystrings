
/**
 * Utility class to do all the work in determining whether sentences or individual words are lucky
 * All methods are used statically by design
 */
public class LuckyCounter {
	
	/**
	 * Takes a single word and returns true if lucky, false otherwise
	 * This is based on the criteria given. You can also define own criteria using overload
	 * @param s must be a single word, lowercase only
	 * @return true if lucky
	 * @throws Exception
	 *             if word is not lowercase or contains spaces (a-z only)
	 */
	public static boolean isLucky(final String s) throws Exception { 
		// static class since seems more like a utility function

		if ((s.isEmpty()) || (s == null))
			throw new Exception("lowercase whole-words only please");
		if (s.length()<4) //shortcut
			return false;
		
		// check string is lowercase a-z
		String regex = "^[a-z]+$";
		if (!s.matches(regex))
			throw new Exception("lowercase whole-words only please");
		
		//note, Regex's aren't that fast. If this is strictly performance-oriented, consider pre-filtering input
		
		int luck = 0;
		
		//Note that iterating through chars of string can be done in many ways
		//For words, what is below should be quite adequte
		//For very large strings, see http://stackoverflow.com/questions/8894258/fastest-way-to-iterate-over-all-the-chars-in-a-string
		
		for (int i=0;i<s.length();++i) {
			if (((s.charAt(i) - 'a') % 2) == 0) // the criterion given
			{
				luck++;
				if (luck == 4)
					return true; //no point iterating through whole word
			}
		}
		return false;
	}

	//Internal helper function so as not to duplicate code
	private static boolean CheckSentence(String sentence)
	{
		if ((sentence==null)||(sentence.isEmpty()))
			return false;
		String regex = "^[a-z ]+$";
		return sentence.matches(regex);
	}
	
	/**
	 * Takes a whole sentence and returns its lucky count based on # of lucky
	 * words. This uses the isLucky method internally
	 * 
	 * @param sentence Must be lowercase a-z only and spaces. No full-stop or anything.
	 * @return lucky count of sentence as integer. -1 means error in input
	 */
	public static int getLuckyCount(final String sentence) {
		int luckyCount = 0;
		if (!CheckSentence(sentence))
			return -1;
		try {
			String[] words = sentence.split(" ");
			for (String word : words) {
				if (isLucky(word)) {
					luckyCount++;
				}
			}
			return luckyCount;
		} catch (Exception ex) { //note this really shouldn't happen since we check sentences
			return -1; // error status. All errors will return -1, you could
						// rather try enable some sort of reporting e.g.
						// "out of range"
		}
	}

	/**
	 * Version of getLuckyCount that allows a user-defined criteria for "luckiness"
	 * @param sentence The sentence to count luckiness for
	 * @param c a user-defined luckiness criteria based on Criteria interface
	 * @return -1 on badly formatted input, otherwise the luckiness count of the sentence
	 */
	public static int getLuckyCount(final String sentence, Criteria c) {
		int luckyCount = 0;
		if (!CheckSentence(sentence))
			return -1;
		try {
			String[] words = sentence.split(" ");
			for (String word : words) {
				if (c.isLucky(word)) {
					luckyCount++;
				}
			}
			return luckyCount;
		} catch (Exception ex) {
			return -1; // error status. All errors will return -1, you could
						// rather try enable some sort of reporting e.g.
						// "out of range"
		}
	}

	/**
	 * This will take an array of sentences and print the luckiest two to the System.out
	 * @param sentences Must be at least 3. Badly formed sentences are not rejected but will score -1
	 * @return No output, prints to System.out. I added nanoTime return just for profiling in test
	 */
	public static Long printLuckiest(final String[] sentences) {

		Long startTime=System.nanoTime(); //using timing only for the tests
		if ((sentences==null)||(sentences.length<3))
			System.out.println("Please use at least three sentences");
		String topstring="";
		String secondstring="";
		int topcount_val=0;
		int secondcount_val=0;
		for (String sentence : sentences) {
			int temp=getLuckyCount(sentence);
			
			//again ignoring cases with same score. You could call it a tie?
			//it is faster to check as we go than to sort after, as sentence set size increases
			if (temp>topcount_val) //new high score
			{
				//replace second
				secondcount_val=topcount_val;
				secondstring=topstring;
				//replace top
				topstring=sentence;
				topcount_val=temp;
			}
			else if (temp>secondcount_val) //new second-highest
			{
				//just replace second
				secondstring=sentence;
				secondcount_val=temp;
			}
		}
		
		//keeping track of only the top-two scorers is much faster than keeping a list of all scores
		//then sorting after
		System.out.println(topstring+" - count " + topcount_val);
		System.out.println(secondstring+" - count " + secondcount_val);
		Long endTime=System.nanoTime();
		Long totalTime=endTime-startTime;
		System.out.println("Time: " + Long.toString(endTime-startTime));
		return totalTime;
	}
	
	/**
	 * Not part of solution. For test purposes only (Complexity issue)
	 * @param sentences
	 * Returning nanotime count just for comparison test
	 */
	public static Long OneLoop(String[] sentences)
	{
		Long startTime=System.nanoTime();
		//keeping track of the top two is faster than storing all then sorting
		String topstring="";
		String secondstring="";
		int topcount_val=0;
		int secondcount_val=0;
		
		//large number of breaks and continues boost speed and increase nonlinearity of solution
		for (String sentence:sentences)
		{
			if (!CheckSentence(sentence))
				continue;
			String[] words=sentence.split(" ");
			if (words.length<=secondcount_val) //impossible to be in top-two
				continue;
			int sentenceLuck=0; //how lucky is this sentence
			for (String word:words)
			{
				//see http://stackoverflow.com/questions/8894258/fastest-way-to-iterate-over-all-the-chars-in-a-string
				//.charAt wins on small words
				int len = word.length();
				if (len<4)
					continue; //ignore these. Can't be lucky
				int wordLuck=0;
				for (int i=0;i<len;++i)
				{
					if (((word.charAt(i) - 'a') % 2) == 0) // the criterion given
					{
						wordLuck++;
						if (wordLuck == 4)//lucky word
						{
							sentenceLuck++;
							break; //skip remaining characters
						}
					}
				}
			}
			
			//check sentenceLuck
			if (sentenceLuck>topcount_val) //new high score. we do not consider ties
			{
				//replace second
				secondcount_val=topcount_val;
				secondstring=topstring;
				//replace top
				topstring=sentence;
				topcount_val=sentenceLuck;
			}
			else if (sentenceLuck>secondcount_val) //new second-highest. No ties
			{
				//just replace second
				secondstring=sentence;
				secondcount_val=sentenceLuck;
			}
		}
		System.out.println(topstring+" - count " + topcount_val);
		System.out.println(secondstring+" - count " + secondcount_val);
		Long endTime=System.nanoTime();
		Long totalTime=endTime-startTime;
		System.out.println("OneLoop Time: " + Long.toString(endTime-startTime));
		return totalTime;
	}
	
	
}

	
