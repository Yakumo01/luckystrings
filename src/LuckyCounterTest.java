import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.concurrent.Callable;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;

import org.junit.Test;


public class LuckyCounterTest {
	static Random random=new Random();
	public String fulltext="it is a truth universally acknowledged that a single man in possession of a good fortune must be in want of a wife.however little known the feelings or views of such a man may be on his first entering a neighbourhood this truth is so well fixed in the minds of the surrounding families that he is considered the rightful property of some one or other of their daughters.my dear mr. bennet said his lady to him one day have you heard that netherfield park is let at last.mr. bennet replied that he had not.but it is returned she. for mrs. long has just been here and she told me all about it.mr. bennet made no answer.do you not want to know who has taken it. cried his wife impatiently.you want to tell me and i have no objection to hearing it.this was invitation enough.why my dear you must know mrs. long says that netherfield is taken by a young man of large fortune from the north of england. that he came down on monday in a chaise and four to see the place and was so much delighted with it that he agreed with mr. morris immediately. that he is to take possession before michaelmas and some of his servants are to be in the house by the end of next week.what is his name.bingley.is he married or single.oh. single my dear to be sure. a single man of large fortune. four or five thousand a year. what a fine thing for our girls.how so. how can it affect them.my dear mr. bennet replied his wife how can you be so tiresome. you must know that i am thinking of his marrying one of them.is that his design in settling here.design. nonsense how can you talk so. but it is very likely that he may fall in love with one of them and therefore you must visit him as soon as he comes.i see no occasion for that. you and the girls may go or you may send them by themselves which perhaps will be still better for as you are as handsome as any of them mr. bingley may like you the best of the party.my dear you flatter me. i certainly have had my share of beauty but i do not pretend to be anything extraordinary now. when a woman has five grown-up daughters she ought to give over thinking of her own beauty.in such cases a woman has not often much beauty to think of.but my dear you must indeed go and see mr. bingley when he comes into the neighbourhood.it is more than i engage for i assure you.but consider your daughters. only think what an establishment it would be for one of them. sir william and lady lucas are determined to go merely on that account for in general you know they visit no newcomers. indeed you must go for it will be impossible for us to visit him if you do not.you are over-scrupulous surely. i dare say mr. bingley will be very glad to see you. and i will send a few lines by you to assure him of my hearty consent to his marrying whichever he chooses of the girls. though i must throw in a good word for my little lizzy.i desire you will do no such thing. lizzy is not a bit better than the others. and i am sure she is not half so handsome as jane nor half so good-humoured as lydia. but you are always giving her the preference.they have none of them much to recommend them replied he. they are all silly and ignorant like other girls. but lizzy has something more of quickness than her sisters.mr. bennet how can you abuse your own children in such a way. you take delight in vexing me. you have no compassion for my poor nerves.you mistake me my dear. i have a high respect for your nerves. they are my old friends. i have heard you mention them with consideration these last twenty years at least.ah you do not know what i suffer.but i hope you will get over it and live to see many young men of four thousand a year come into the neighbourhood.it will be no use to us if twenty such should come since you will not visit them.depend upon it my dear that when there are twenty i will visit them all.mr. bennet was so odd a mixture of quick parts sarcastic humour reserve and caprice that the experience of three-and-twenty years had been insufficient to make his wife understand his character. her mind was less difficult to develop. she was a woman of mean understanding little information and uncertain temper. when she was discontented she fancied herself nervous. the business of her life was to get her daughters married. its solace was visiting and news. bennet was among the earliest of those who waited on mr. bingley. he had always intended to visit him though to the last always assuring his wife that he should not go. and till the evening after the visit was paid she had no knowledge of it. it was then disclosed in the following manner. observing his second daughter employed in trimming a hat he suddenly addressed her with.i hope mr. bingley will like it lizzy.we are not in a way to know what mr. bingley likes said her mother resentfully since we are not to visit.but you forget mamma said elizabeth that we shall meet him at the assemblies and that mrs. long promised to introduce him.i do not believe mrs. long will do any such thing. she has two nieces of her own. she is a selfish hypocritical woman and i have no opinion of her.no more have i said mr. bennet. and i am glad to find that you do not depend on her serving you.mrs. bennet deigned not to make any reply but unable to contain herself began scolding one of her daughters.dont keep coughing so kitty for heavens sake. have a little compassion on my nerves. you tear them to pieces.kitty has no discretion in her coughs said her father. she times them ill.i do not cough for my own amusement replied kitty fretfully. when is your next ball to be lizzy.to-morrow fortnight.aye so it is cried her mother and mrs. long does not come back till the day before. so it will be impossible for her to introduce him for she will not know him herself.then my dear you may have the advantage of your friend and introduce mr. bingley to her.impossible mr. bennet impossible when i am not acquainted with him myself. how can you be so teasing.i honour your circumspection. a fortnights acquaintance is certainly very little. one cannot know what a man really is by the end of a fortnight. but if we do not venture somebody else will. and after all mrs. long and her nieces must stand their chance. and therefore as she will think it an act of kindness if you decline the office i will take it on myself.the girls stared at their father. mrs. bennet said only nonsense nonsense.what can be the meaning of that emphatic exclamation. cried he. do you consider the forms of introduction and the stress that is laid on them as nonsense. i cannot quite agree with you there. what say you mary. for you are a young lady of deep reflection i know and read great books and make extracts.mary wished to say something sensible but knew not how.while mary is adjusting her ideas he continued let us return to mr. bingley.i am sick of mr. bingley cried his wife.i am sorry to hear that. but why did not you tell me that before. if i had known as much this morning i certainly would not have called on him. it is very unlucky. but as i have actually paid the visit we cannot escape the acquaintance now.the astonishment of the ladies was just what he wished. that of mrs. bennet perhaps surpassing the rest. though when the first tumult of joy was over she began to declare that it was what she had expected all the while.how good it was in you my dear mr. bennet. but i knew i should persuade you at last. i was sure you loved your girls too well to neglect such an acquaintance. well how pleased i am. and it is such a good joke too that you should have gone this morning and never said a word about it till now.now kitty you may cough as much as you choose said mr. bennet. and as he spoke he left the room fatigued with the raptures of his wife.what an excellent father you have girls. said she when the door was shut. i do not know how you will ever make him amends for his kindness. or me either for that matter. at our time of life it is not so pleasant i can tell you to be making new acquaintances every day. but for your sakes we would do anything. lydia my love though you are the youngest i dare say mr. bingley will dance with you at the next ball.oh. said lydia stoutly i am not afraid. for though i am the youngest im the tallest.the rest of the evening was spent in conjecturing how soon he would return mr. bennets visit and determining when they should ask him to dinner.";
	/**
	 * Basic test checking that words of the correct makeup are counted as 'lucky'
	 */
	@Test
	public void testWords() {
		final String alphabet="abcdefghijklmnopqrstuvwxyz";
		for (char l:alphabet.toCharArray())
		{
			if ((l-'a')%2==0)
			{
				try {
					assertTrue(LuckyCounter.isLucky(new String(new char[4]).replace('\0', l)));
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
			else
			{
				try {
			
				assertFalse(LuckyCounter.isLucky(new String(new char[4]).replace('\0', l)));
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
				
		}
	}
	
	/**
	 * Some sentences where we can easily get the count in our head and check it concurs
	 */
	@Test 
	public void testSentenceCount()
	{
		String sentence1="aaaaa cccccc eeeee";
		String sentence2="b aa kkkkkkk";
		String sentence3="cccccccc cdaa mmmmmmm m";
		assertTrue(LuckyCounter.getLuckyCount(sentence1)==3);
		assertTrue(LuckyCounter.getLuckyCount(sentence2)==1);
		assertTrue(LuckyCounter.getLuckyCount(sentence3)==2);
	}
	
	/**
	 * A few sentences where we can easily check the output ourselves
	 */
	@Test
	public void testSentences() {
		final String sentences[]={"aaaa bbbb cccc dddd", "aaaa aaaaah aaaar aaaaaaarrgghh", "bbbbb is in b", "aaaaa cccc"};
		LuckyCounter.printLuckiest(sentences); //we know the answer
	}
	
	/**
	 * Helper class to get random strings to test
	 * @return a random sentence with only a-z and space
	 */
	private static String getRandomSentence()
	{
		char[] chars = "abcdefghijklmnopqrstuvwxyz".toCharArray();
		StringBuilder sb = new StringBuilder();
		
		//we could really just use one random. It's not a scientific test!
		
		int sentenceLength=random.nextInt(10);//max 10 word sentences
		
		for (int j=0;j<sentenceLength;++j)
		{
			int wordLength = random.nextInt(20); //max 20 character words
			for (int i = 0; i <wordLength ; i++) { //word loop
			    char c = chars[random.nextInt(chars.length)];
			    sb.append(c); //random word
			}
			
			if (j!=sentenceLength-1) //space between words
				sb.append(" ");
		}
		
		return sb.toString();
	}
	
	
	
	/**
	 * Test that badly formed sentences fail gracefully
	 */
	@Test public void FailSentences()
	{
		String goodSentence="abc def ghi jkl mno pqrst uv w zyz";
		String badSentence1="ab de.";
		String badSentence2="abc1 def2";
		String badSentence3="Abc1-def2";
		String badSentence4="Abc1       def2";
		assert(LuckyCounter.getLuckyCount(badSentence1)==-1);
		assert(LuckyCounter.getLuckyCount(badSentence2)==-1);
		assert(LuckyCounter.getLuckyCount(badSentence3)==-1);
		assert(LuckyCounter.getLuckyCount(badSentence4)==-1);
		assert(LuckyCounter.getLuckyCount(goodSentence)>-1);
	}
	
	/**
	 * Test that Custom Criteria gives same result as normal (fixed in code) way
	 */
	@Test
	public void TestCutomCriteria()
	{
		//100 iterations because it's easy to get the same result randomly
		for (int i=0;i<100;++i){
		String sentence=getRandomSentence();
		assert(LuckyCounter.getLuckyCount(sentence)==LuckyCounter.getLuckyCount(sentence, new LuckyAmazonCriteria()));}
	}
	
	/**
	 * Test that bad words fail word regex filter
	 */
	@Test 
	public void FailRegex()
	{
		int exceptionCount=0;
		String badString1="1234";
		String badString2="Hello";
		String badString3="no spaces";
		String badString4="no@symbols$";
		String goodString="abcdefghijklmnopqrstuvwxyz";
		
		try {
		LuckyCounter.isLucky(badString1);
		} catch (Exception ex)
		{
			exceptionCount++;
		}
		try {
			LuckyCounter.isLucky(badString2);
			} catch (Exception ex)
			{
				exceptionCount++;
			}
		try {
			LuckyCounter.isLucky(badString3);
			} catch (Exception ex)
			{
				exceptionCount++;
			}
		try {
			LuckyCounter.isLucky(badString4);
			} catch (Exception ex)
			{
				exceptionCount++;
			}
		try {
			LuckyCounter.isLucky(goodString);
			} catch (Exception ex)
			{
				exceptionCount++;
			}
		assert(exceptionCount==4);
		
	}
	
	/**
	 * Compare speed of one loop execution to neat execution
	 * Data is synthetic, might be good to use a book or something
	 */
	@Test
	public void CompareOneLoop()
	{
		final int SENTENCECOUNT=1000;
		double Total=0;
		for (int j=0;j<100;++j) //repeat test 100 times
		{
			String[] sentenceList=new String[SENTENCECOUNT];
			for (int i=0;i<SENTENCECOUNT;++i)
			{
				sentenceList[i]=getRandomSentence();
			}
			
			Long OneCount=LuckyCounter.OneLoop(sentenceList);
			Long NeatCount=LuckyCounter.printLuckiest(sentenceList);
			Total+=(NeatCount/OneCount);
			
		}
		Total/=100f; //average improvement
		System.out.println("Over 100 random loops, Tight loop was " + Double.toString(Total) + " times faster");
	}
	

	@Test
	public void PrejudiceTest()
	{
		
		String[] sentences=fulltext.split("\\.");
		System.out.println("Sentences: " +sentences.length);
		Long OneCount=LuckyCounter.OneLoop(sentences);
		Long NeatCount=LuckyCounter.printLuckiest(sentences);
		System.out.println("NeatCount/OneCount is " + Double.toString((double)NeatCount/(double)OneCount));
		
	}
	
	@Test
	public void OrderTest()
	{
		Map<Long,Integer> countMap=new HashMap<Long,Integer>(0); //store times and sentence lengths
		String[] sentences=fulltext.split("\\.");
		System.out.println("Sentences: " +sentences.length);
		for (String sentence:sentences)
		{
			String inSentence=sentence.trim();
			//how many words?
			int wordcount=inSentence.split(" ").length;
			Long startTime=System.nanoTime();
			int score=LuckyCounter.getLuckyCount(inSentence);
			Long endTime=System.nanoTime();
			System.out.println(inSentence);
			System.out.println(inSentence.length());
			System.out.println(score);
			countMap.put(endTime-startTime,wordcount); //try with sentence length also
		}
		
		//now check out how it scales
		countMap=sortByValue(countMap); //sort by sentence length
		
		for(Map.Entry<Long,Integer> entry : countMap.entrySet()) {
			System.out.println("Key: " +entry.getKey() + " Value: " + entry.getValue());
		}
		
	}
	
	public static <K, V extends Comparable<? super V>> Map<K, V> sortByValue(
			Map<K, V> map) {
		List<Map.Entry<K, V>> list = new LinkedList<Map.Entry<K, V>>(
				map.entrySet());
		Collections.sort(list, new Comparator<Map.Entry<K, V>>() {
			public int compare(Map.Entry<K, V> o1, Map.Entry<K, V> o2) {
				return (o1.getValue()).compareTo(o2.getValue());
			}
		});

		Map<K, V> result = new LinkedHashMap<K, V>();
		for (Map.Entry<K, V> entry : list) {
			result.put(entry.getKey(), entry.getValue());
		}
		return result;
	}
}
